
const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");

let image = new Image();
image.src = "assets/img/Green-16x18-spritesheet.png";
//image.onload = gameloop;

let collisions = false;
let playerEnemyCollisions = false;
const scale = 8;
const width = 16;
const height = 18;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 0, 2];
let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let isMoving = false;

let actions = true;

let image2 = new Image();
image2.src = "assets/img/heen.png";//gets image from assets folder

let image3 = new Image();
image3.src = "assets/img/turniphead.png";//gets image from assets folder

let audio=new Audio("assets/audio/bonk.mp3");

function drawFrame(frameX, frameY, canvasX, canvasY) 
{
    context.drawImage(image,frameX * width, frameY * height, width, height, canvasX, canvasY, scaledWidth, scaledHeight);
}

function GameObject(spritesheet, x, y, width, height)
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

let player = new GameObject(image, 25,25, 200, 200);
let enemy1 = new GameObject(image2, 700, 150, 200, 200);
let enemy2 = new GameObject(image3, 350, 220, 200, 300);

function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input


function input(event) {
    // Take Input from the Player
    // console.log("Input");
   // console.log(event);
   // console.log("Event type: " + event.type);
    // console.log("Keycode: " + event.key);

    if (event.type === "keydown") {
        switch (event.key) {
            case "ArrowLeft": // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case "ArrowUp": // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case "ArrowRight": // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case "ArrowDown": // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            case "r": //randomise enemy positions
                gamerInput = new GamerInput("r");
                break;
            //case "c": //change picture
               // gamerInput = new GamerInput("c");
               // break;
            case "s": //resize heen
                gamerInput = new GamerInput("s");
                break;
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
        actions = true;
        isMoving=false;
    }
}

function update()
{
    if (gamerInput.action === "Up") 
    {
        console.log("Move Up");
        player.y -= 1; // Move Player Up
        currentDirection=1;
        isMoving=true;
    } 
    else if (gamerInput.action === "Down") 
    {
        console.log("Move Down");
        player.y += 1; // Move Player Down
        currentDirection=0;
        isMoving=true;
    } 
    else if (gamerInput.action === "Left") 
    {
        console.log("Move Left");
        player.x -= 1; // Move Player Left
        currentDirection=2;
        isMoving=true;
    } 
    else if (gamerInput.action === "Right") 
    {
        console.log("Move Right");
        player.x += 1; // Move Player Right
        currentDirection=3;
        isMoving=true;
    }
    if (gamerInput.action === "r"&&actions==true)
    {
        console.log("random");
        enemy1.x = Math.floor(Math.random() * 901);
        enemy1.y = Math.floor(Math.random() * 201);

        enemy2.x = Math.floor(Math.random() * 901);
        enemy2.y = Math.floor(Math.random() * 301);
        actions=false;
    }
    //if (gamerInput.action === "c" && actions == true)
   // {
       // console.log("change");
       // if (player.spritesheet == image)
       // {
       //     player.spritesheet = image4;
       // }
       // else
       // {
        //    player.spritesheet = image;
           
      //  }
       //  actions=false;
   // }
    if (gamerInput.action === "s" && actions == true)
    {
        console.log("resize");
        if(enemy1.width===200 && enemy1.height===200)
        {//rezises enemy 1(heen)
            enemy1.width = 100;
            enemy1.height = 100;
        }
        else{
            enemy1.width=200;
            enemy1.height=200;
        }
        actions = false
    }
    if (collisions==true)
    {
        document.body.style.background = "red";
        console.log("collisions");
    }
    else
    {
        document.body.style.background = "white";
    }
    if(playerEnemyCollisions==true)
    {
        audio.play();
        console.log("player enemy collision");
    }
}

function draw() 
{
    // Clear Canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
    //console.log("Draw");
    //console.log(player);
    context.drawImage(enemy1.spritesheet, enemy1.x, enemy1.y, enemy1.width, enemy1.height);
    context.drawImage(enemy2.spritesheet, enemy2.x, enemy2.y, enemy2.width, enemy2.height);
    drawFrame(walkLoop[currentLoopIndex],currentDirection,player.x,player.y); 
    if(isMoving==true)
    {
         frameCount++;
        if (frameCount < 15) 
        {
         return;
        }
        frameCount = 0;
        currentLoopIndex++;
        if (currentLoopIndex >= walkLoop.length) 
        {
         currentLoopIndex = 0;
        }
    }
    else
    {
        drawFrame(0,currentDirection,player.x,player.y)
        frameCount=0;
        currentLoopIndex=0;
    }
   
    //context.drawImage(player.spritesheet, player.x, player.y, player.width, player.height);
    
}

function gameloop() 
{
    collisionsDetection();
    update();
    draw();
    window.requestAnimationFrame(gameloop);

}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);

function collisionsDetection()
{
    if(player.x>1100||player.x<0)
    {
        collisions=true;
    }
    else if(player.y>500||player.y<0)
    {
        collisions=true;
    }
    else 
    {
        collisions=false;
    }

    if (player.x == enemy1.x ||player.x==enemy2.x)
    {
        playerEnemyCollisions=true;
    }
    else if (player.y==enemy1.y||player.y==enemy2.y)
    {
        playerEnemyCollisions=true;
    }
    else
    {
        playerEnemyCollisions=false;
    }
}
/*function step() 
{
   
     context.clearRect(0, 0, canvas.width, canvas.height);
     
    drawFrame(walkLoop[currentLoopIndex], currentDirection, 0, 0);
     
        console.log("changing direction to dir: " + currentDirection);
        currentDirection++;
      
      if (currentDirection >= 4) {
        currentDirection = 0;
      }
      window.requestAnimationFrame(step);
    }*/